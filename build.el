(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

(package-initialize)
					; (package-refresh-contents)

(unless (package-installed-p 'htmlize)
  (package-refresh-contents)
  (package-install 'htmlize))

(require 'org)
(require 'ox-html)
(require 'ox-publish)
(require 'htmlize)

(setf user-full-name "Pawel Blazejewski")
(setf user-mail-address "pblazh@gmail.com")
(setf org-export-html-coding-system 'utf-8-unix)
(setq org-html-htmlize-output-type "css")

(setq org-publish-project-alist
      '(
	("blog-notes"
	 :base-directory "./posts/"
	 :base-extension "org"
	 :publishing-directory "./public/"
	 :recursive t
	 :publishing-function org-html-publish-to-html
	 :headline-levels 4
	 :with-toc nil
	 :section-numbers nil
	 :with-author t
	 :with-creator nil
	 :with-emphasize t
	 :with-tables t
	 :with-drawers t
	 :with-footnotes t
	 :html-checkbox-type unicode
	 :exclude "sitemap.org\\|.*\.draft\.org"
	 :html-metadata-timestamp-format "%d.%m.%Y"
					; :html-link-home "./index.html"
	 :html-home/up-format "
	    <div id=\"org-div-home-and-up\">
	      <!-- %s -->
	      <a accesskey=\"H\" href=\"%s\"> HOME </a>
	    </div>
	  "

	 :html-doctype "html5"
	 :html-html5-fancy t
	 :html-head-include-default-style nil
	 :html-head-include-scripts nil
	 :htmlized-source t
	 :html-head "
	    <style>
	      body{
		background-color: #282828;
		color: #fbf1c7;
	      }
	    </style>
	 "
	 :html-preamble "
	    <h1>html-preamble </h1>
	 "

	 :html-postamble "<span class=\"author\">%a (%e)<span> <span class=\"date\"> %d</span>"


	 :auto-preamble nil
	 :auto-sitemap t
	 :sitemap-sort-files anti-chronologically
	 )

	("blog-static"
	 :base-directory "./posts/"
	 :base-extension "css\\|png\\|jpg"
	 :publishing-directory "./public/"
	 :recursive t
	 :publishing-function org-publish-attachment
	 )

	("blog" :components ("blog-notes" "blog-static"))

	))
