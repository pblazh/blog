#+TITLE: The curious case of broken Promises
#+DATE: 8.11.2019
#+SETUPFILE: ../level-0.org

#+BEGIN_QUOTE
A broken promise is not a lie.
#+END_QUOTE

Everybody knows that Promises are broken, they are dead and have to be abandoned for the sake of a new Async/Await approach. But is it totally true? What are the objections to promises and are they really that serious?
Here they are:

    - Promises have non-clear complicated syntax.
    - Promises are forcing you to write a lot of boilerplate.
    - Promises are swallowing errors.
    - Promises are always run on the initiation and you can not manage the data flow.

Let’s check.

* The beginning.

Here it begins. Imagine you have a synchronous API. The extremely simple math language: three operations and numbers. And also you have an extremely complex magic formula provided by the best minds of humanity and probably it is even a secret sauce for some revolutionary startup ;-)

#+BEGIN_SRC javascript :tangle yes
x = 2, y = 3
result = (3x + 1) * (x + y) / 2
#+END_SRC

To calculate it we introduced the new arsenal of a data type and three operations. And do not try to understand the formula it is so complex that is absolutely impossible

#+BEGIN_SRC javascript :tangle yes
let num = val => val;
let sum = (a, b) => a + b;
let mul = (a, b) => a * b;
let div = (a, b) => a / b;

// and here the magic begins...
let x = num(2);
let y = num(3);
const result =
    div(
        mul(
            sum(
                mul(num(3), x),
                num(1),
            ),
            sum(x, y),
        ),
        num(2)
    );

console.log('RESULT:', result);
#+END_SRC

So far so good. And suppose, that later we need to make it asynchronous so some numbers are accessible right now but not the others. Those unfortunates have to be calculated remotely and fetched in the exact point where they are really used. Not thinking twice we just added some promises.

#+BEGIN_SRC javascript :tangle yes
const num_async = n => Promise.resolve(num(n));
let x_;
let triple_x_plus_one_;

num_async(2)
    .then(x => {
        x_ = x;
        triple_x_plus_one_ = sum(mul(x, num(3)), num(1));
    })
    .then(() => num_async(3))
    .then(y => {
        return div(
            mul(
                triple_x_plus_one_,
                sum(x_, y)
            ),
            num(2)
        );
    }).then(
        value => console.log('promise RESULT:', value),
        error => console.error('promise ERROR:', error)
    );
#+END_SRC

Yuck. That is not the best but I saw even worse and that is how promises are usually presented in articles about the “async”. You can easily spot all the disadvantages of the promises. They are obscure, bloated and they do have a convoluted data flow. Ane even worse, we introduced the eternal sin of the global variables and side effects. Did you say, that it could be easily fixed and immediately came up with the smart refactoring? But wait a while, and see what we could achieve if replace the promises with the async.

* Async

#+BEGIN_SRC javascript :tangle yes
const num_async = n => Promise.resolve(num(n));

async function calculate() {
    const x = await num_async(2);
    const triple_x_plus_one = sum(mul(x, num(3)), num(1));
    const y = await num_async(3);
    const result =
    div(
        mul(
            triple_x_plus_one,
            sum(x, y)),
        num(2)
    );
    return result;

}

calculate().then(
    value => console.log('async RESULT:', value),
    error => console.error('async ERROR:', error)
);
#+END_SRC

Better, much better indeed. The flow is much clear: wait for data, calculate, wait again and continue calculations. We’ve been even forced to hide those ugly variables in the scope of the function. We could go even further and inline awaits into the formula. Variables disappeared. The “x” is fetched twice but who cares.

#+BEGIN_SRC javascript :tangle yes
const result =
    div(
        mul(
            sum(
                mul(num(3), await num_async(2)),
                num(1),
            ),
            sum(await num_async(2), await num_async(3)),
        ),
        num(2)
    );
#+END_SRC

That’s a silver bullet indeed. Let’s use more of them, kill the promises. Kill them all.

* But

Huston, we still have a problem. When we were introducing the asynchronicity, we were made to rewrite our secret formula. It needs to be divided into smaller chunks depending on the waiting points. So if just another variable goes wild and becomes async we will have to rewrite it again. So what changed? We replaced the declarative expression with the imperative flow. Tried to introduce the flow using promises, and failed, and only async/await save us from the total disgrace. How come that we didn’t have and didn’t need the flow before?
And perhaps, that is the answer. Promises are not about a flow. They are declarative. They are just a data type. Earlier we had strings, numbers, booleans and now we also could have “promises”. We could have put our primitives into the object wrappers like new String or new Boolean, and that is exactly the same.
So let’s replace our constructor ‘num’ with a new one.

| num = value => Promise.resolve(value);

We have a new data type now, and as a consequence, we need to define operations on that data type, not the numbers. Luckily for us, there is a map operation on that type. It’s called “then” and we could easily map all our operations on the new data type.

#+BEGIN_SRC javascript :tangle yes
const waitAnd = op => (...promised) => Promise
    .all(promised)                       // wait for operands
    .then(values => values.reduce(op));  // and do the calculation

sum = waitAnd(sum);
mul = waitAnd(mul);
div = waitAnd(div);
#+END_SRC

That's all. We run the magic formula again and it works!

#+BEGIN_SRC javascript :tangle yes
const result =
    div(
        mul(
            sum(
                mul(num(3), x),
                num(1),
            ),
            sum(x, y),
        ),
        num(2)
    );

result.then(
    value => console.log('promise RESULT:', value),
    error => console.error('promise ERROR:', error)
);
#+END_SRC

No changes, no flow, exactly the same code as before. The only minor detail is we can not use the console.log to display our values but it’s understandable. Now, ‘num’ is a new data type and the ‘console.log’ does not support it from scratch. It has to be adapted as well.

#+BEGIN_SRC javascript :tangle yes
const log = promise => promise.then(console.log);
const err = promise => promise.catch(console.error);
#+END_SRC

If we need to fetch some value or delay it, or something we could just wrap it as well.

#+BEGIN_SRC javascript :tangle yes
let delayed = value => new Promise((resolve, reject) => {
   setTimeout(resolve, 500, value);
   //setTimeout(reject, 1000, `Delayed ${value} does not exist`);
});
#+END_SRC

So the formula is now:

#+BEGIN_SRC javascript :tangle yes
const result =
    div(
        mul(
            sum(
                mul(num(3), x),
                num(delayed(1)),
            ),
            sum(x, y),
        ),
        num(2)
    );
#+END_SRC

* Easy peasy.

And now about the disadvantage of promises I haven’t mentioned before. The curious case of swallowed errors. The main objection was, that you could have an error somewhere in your flow, and if you haven’t attached a catch function to your promise, you wouldn’t even notice. The error is swallowed. But here is the answer: promises are not about a flow. A Promise is a value. A value could be correct or wrong, but you won’t know until you check it. That is the exactly the same story a NaN. NaN is a value, not a flow. If you have an arithmetical expression its’ value could be a number or not a number.
If you change the ‘delayed’ function above and make it reject with an error the whole expression is to be rejected with the same error. Why do not throw here? Because that is a part which produces values, correct or not. Actually, the coolest thing about promises, that you can return them from the function as any other value. Where to throw? Perhaps in the place where the value is checked.

#+BEGIN_SRC javascript :tangle yes
const wait = msec =>
   new Promise((_, reject) => {
      setTimeout(reject, msec, 'Too late');
   }
);

const fastResult = Promise.race([
    wait(100),
    result,
]);
#+END_SRC

If the formula failed to calculate the result in 100 ms, the value is not correct and we don’t need it anymore, even though, it contains some numbers inside. The formula itself doesn’t know about such restrictions and could not attach that “catch” itself.

The only sad thing in this story that this “error” is already “fixed” and you have to attach ‘catch’ whether need it or not.

* The emerging

The promises are run on the creation time, and that is true. Probably it would be better of them to run on request, and that could also be true. The good news is that you could easily add this behavior yourself, and it’s called a “thunk”. And this is not about the Redux library at all.
A thunk is a value wrapped in a function to make it lazy.
So let’s wrap our numbers in a function. Some tiny changes.

#+BEGIN_SRC javascript :tangle yes
const runAndWaitAnd = op => (...promised) => () =>
    Promise
        .all(promised.map(thunk => thunk()))
        .then(values => values.reduce(op));

num = val => () =>
    console.log('calculate:', val) || Promise.resolve(val);

sum = runAndWaitAnd(sum);
mul = runAndWaitAnd(mul);
div = runAndWaitAnd(div);
#+END_SRC

A few small changes, and now our formula does not autostart. It’s just a formula. But look, the formula is exactly the same, nothing changed at all.

#+BEGIN_SRC javascript :tangle yes
const result =
    div(
        mul(
            sum(
                mul(num(3), x),
                num(delayed(1)),
            ),
            sum(x, y),
        ),
        num(2)
    );
#+END_SRC

And silence, and darkness of the prehistorical space. And a call. And then BANG, it was run.

| log(result());

* And now something completely different.

The num is now a function for calculating a number. Let’s suppose, it’s a hard remote operation, and we don’t want to run it twice. Could we store the return value of a function and reuse it? Absolutely, just use the old “num” constructor and add the cache for results.

#+BEGIN_SRC javascript :tangle yes
num = val =>
    console.log(`Calculating: ${value}`)
    || Promise.resolve(val);

function cacheThunks(calculate){
    const cache = new Map();

    return function (value){
        if(!cache.has(value)){
            const result = calculate(value);
            cache.set(value, () => result);
        }

        return cache.get(value);
    }
}

num = cacheThunks(num);
#+END_SRC

But look isn’t that an old friend ‘memoize’? Almost. We need to memorize the result of a function which is called only once. So if you have the full arsenal, it could be expressed in a much much shorter way.

| num = memoize(once(num));

That’s all. And the best thing is that THE FORMULA is still intact.

* Is the Async/Await is dead?

Probably no. Is it a replacement for the Promise? Not likely.
Is the Promise bad for the flow control? It looks like yes, but it was not invented for that. If you need an imperative flow, use async but if you need a value, use promises.

#+BEGIN_SRC javascript :tangle yes
async function brewTheSecretSauce() {
    try {
        /* step 1 */ const result = await formula();
        /* step 2 */ validate(result);
        /* step 3 */ return use(result);
    } catch (error) {
        show(error);
    }
}
#+END_SRC

And remember, when returning a value you still return a Promise. And by the way, it’s a good idea to always return a value of the same type. And what did you intend to return from the catch branch?

If there any cases when promises could be used for the flow? Yes, if you could express the flow in a declarative manner, but this a different story.

#+BEGIN_SRC javascript :tangle yes
const brew = composeSteps(validate, formula, prepare);
log(brew());
#+END_SRC

* Let us promise to Promise wisely.
