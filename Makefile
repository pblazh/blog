# Makefile for myblog

.PHONY: all build build_no_init

all: build

build: build.el
	@echo "Publishing... with current Emacs configurations."
	emacs --batch --load build.el --funcall org-publish-all

build_no_init: build.el
	@echo "Publishing... with --no-init."
	emacs --batch --no-init --load build.el --funcall org-publish-all

clean:
	@echo "Cleaning up.."
	@rm -rvf *.elc
	@rm -rvf public
	@rm -rvf ~/.org-timestamps/*
